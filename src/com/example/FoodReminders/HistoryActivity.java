package com.example.FoodReminders;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.*;
import android.widget.AdapterView.OnItemSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class HistoryActivity extends Activity implements OnItemSelectedListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.history);

        Spinner spinner=(Spinner) findViewById(R.id.history_spin);
        spinner.setOnItemSelectedListener(this);

        List<String> categories = new ArrayList<>();
        categories.add("Today");
        categories.add("This week");
        categories.add("This month");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);

        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/HoneyScript-Light.ttf");
        TextView history = (TextView) findViewById(R.id.history);
        history.setTypeface(face);
        TextView more = (TextView) findViewById(R.id.moreHistoryButton);
        more.setTypeface(face);

        //demo
        Float ratio = 2.5f;
        //Integer ratio = getHistory(text); commented out for demoing
        TextView historyRatio = (TextView) findViewById(R.id.history_ratio);
        historyRatio.setText(ratio.toString() + "/5");
        historyRatio.setTypeface(face);
    }

    @Override //TODO
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        //still demo
        Float ratio = 0.5f;
        if("This week".equals(item)) {
            ratio = 5f;
        } else if ("This month".equals(item)) {
            ratio = 3.6f;
        }
        TextView historyRatio = (TextView) findViewById(R.id.history_ratio);
        historyRatio.setText(ratio.toString() + "/5");

        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    public void onNothingSelected(AdapterView<?> arg) {

    }

    /** Called when the user clicks the Detailed history button */
    public void openPurchase(View view) {
        Intent intent = new Intent(this, PurchaseActivity.class);
        startActivity(intent);
    }

    private Integer getHistory(String since) {
        SQLiteDatabase db = openOrCreateDatabase("foodreminder", MODE_PRIVATE, null);
        String historySince = getTimeSince(since);

        Cursor resultSet = db.rawQuery("Select * from History where time > '" + historySince + "'",null);
        resultSet.moveToFirst();

        Integer allTimes = resultSet.getCount();
        Integer successfulTimes = 0;

        while(resultSet.getCount() != 0 && !resultSet.isLast()) {
            if(!resultSet.isNull(2)) {
                successfulTimes += resultSet.getInt(2); //this should be bit, so unsuccessful times are not added
            }
            resultSet.move(1);
        }
        resultSet.close();
        return allTimes == 0 ? 0 : successfulTimes/allTimes*5;
    }

    private String getTimeSince(String since) {
        Calendar cal = Calendar.getInstance();
        cal.setFirstDayOfWeek(Calendar.MONDAY);

        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);

        if("This week".equals(since)) {
            cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        } else if ("This month".equals(since)) {
            cal.set(Calendar.DAY_OF_MONTH, 1);
        }

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
        return format.format(cal.getTime());
    }
}
