package com.example.FoodReminders;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;

public class ReminderActivity extends Activity implements AdapterView.OnItemSelectedListener {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reminders);

        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/HoneyScript-Light.ttf");
        TextView time = (TextView) findViewById(R.id.timeOne);
        time.setTypeface(face);
        TextView type = (TextView) findViewById(R.id.typeOne);
        type.setTypeface(face);
        TextView txt = (TextView) findViewById(R.id.textOne);
        txt.setTypeface(face);
        Button bt = (Button) findViewById(R.id.addReminderButton);
        bt.setTypeface(face);
        setDemoData();
    }

    public void openNew(View view) {
        //this is for demo purposes, to see the popup. In real app a new reminder creation would start
        Intent intent = new Intent(this, PopupActivity.class);
        startActivity(intent);
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        // On selecting a spinner item
        String item = parent.getItemAtPosition(position).toString();
        // Showing selected spinner item
        Toast.makeText(parent.getContext(), "Selected: " + item, Toast.LENGTH_LONG).show();
    }

    public void onNothingSelected(AdapterView<?> arg) {

    }

    private void setDemoData() {
        Spinner spinner = (Spinner) findViewById(R.id.dayOne);
        spinner.setOnItemSelectedListener(this);
        List<String> categories = new ArrayList<String>();
        categories.add("Monday");
        categories.add("Tuesday");
        categories.add("Wednesday");
        categories.add("Thursday");
        categories.add("Friday");
        categories.add("Saturday");
        categories.add("Sunday");
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setSelection(1);
    }
}
