package com.example.FoodReminders;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class PopupActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.popup);

        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/HoneyScript-Light.ttf");
        TextView tx = (TextView) findViewById(R.id.txt);
        Button done = (Button) findViewById(R.id.done);
        Button nah = (Button) findViewById(R.id.nah);
        tx.setTypeface(face);
        done.setTypeface(face);
        nah.setTypeface(face);
    }
}
