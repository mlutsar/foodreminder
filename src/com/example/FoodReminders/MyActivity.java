package com.example.FoodReminders;

import android.app.Activity;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initDatabaseIfNotExists();
        setContentView(R.layout.main);

        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/HoneyScript-Light.ttf");
        Button rem = (Button) findViewById(R.id.reminderButton);
        Button his = (Button) findViewById(R.id.historyButton);
        Button men = (Button) findViewById(R.id.menuButton);
        rem.setTypeface(face);
        his.setTypeface(face);
        men.setTypeface(face);
    }
	
	/** Called when the user clicks the Reminders button */
    public void openReminders(View view) {
        Intent intent = new Intent(this, ReminderActivity.class);
        startActivity(intent);
    }
	
	/** Called when the user clicks the History button */
    public void openHistory(View view) {
        Intent intent = new Intent(this, HistoryActivity.class);
        startActivity(intent);
    }

    /** Called when the user clicks the Menus button */
    public void openPurchase(View view) {
        Intent intent = new Intent(this, PurchaseActivity.class);
        startActivity(intent);
    }

    private void initDatabaseIfNotExists() {
        SQLiteDatabase db = openOrCreateDatabase("foodreminder",MODE_PRIVATE,null);

        db.execSQL("CREATE TABLE IF NOT EXISTS Reminders(" +
                "mon BIT, tue BIT, wed BIT, thu BIT, fri BIT, sat Bit, sun BIT" +
                "time TIME, type TINYINT, text VARCHAR);");
        db.execSQL("CREATE TABLE IF NOT EXISTS History(time DATETIME, done BIT);");
    }
}
