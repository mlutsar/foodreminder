package com.example.FoodReminders;


import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.widget.TextView;

public class PurchaseActivity extends Activity{

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       setContentView(R.layout.purchase);

        Typeface face= Typeface.createFromAsset(getAssets(), "fonts/HoneyScript-Light.ttf");
        TextView txt = (TextView) findViewById(R.id.txt);
        txt.setTypeface(face);
    }
}
